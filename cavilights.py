#!/usr/bin/python

import time
from neopixel import *
import argparse

TOP_RIGHT_LIGHT = 0
TOP_LEFT_LIGHT = 1
BOTTOM_LIGHT = 2

def main():

  parser = argparse.ArgumentParser()
  parser.add_argument('-t', '--test', action='store_true', help='test the lights')
  parser.add_argument('-l', '--light', help='set the light', type=int)
  parser.add_argument('-o', '--on', action='store_true', help='set light on')
  parser.add_argument('-f', '--off', action='store_true', help='set light off')
  args = parser.parse_args()

  try:
    cavi_lights = CaviLights()

    if args.test:
      if args.light == None:
        print ("Running lights test")
        cavi_lights.light_party()
      else:
        print ("Running light " + str(args.light) + " test")
        cavi_lights.cycle_light_colours(args.light, True)
    else:
      if args.light == None:
        if args.off:
          print ("Turning all lights off")
          cavi_lights.lights_off()
      else:
        if args.on:
          print ("Turning light " + str(args.light) + " on")
          cavi_lights.light_on(args.light, True)
        elif args.off:
          print ("Turning light " + str(args.light) + " off")
          cavi_lights.light_on(args.light, False)
        
  except KeyboardInterrupt:
    print ("Turning lights off")
    cavi_lights.lights_off()
        
class CaviLights:
  
  def __init__(self):

    self.LED_PIN         = 18
    self.LED_FREQ_HZ     = 800000  # LED signal frequency in hertz (usually 800khz)
    self.LED_DMA         = 10      # DMA channel to use for generating signal (try 10)
    self.LED_BRIGHTNESS  = 255     # Set to 0 for darkest and 255 for brightest
    self.LED_INVERT      = False   # True to invert the signal (when using NPN transistor level shift)
    self.LED_CHANNEL     = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53

    self.lights = Adafruit_NeoPixel(3, self.LED_PIN, self.LED_FREQ_HZ, self.LED_DMA, self.LED_INVERT, self.LED_BRIGHTNESS, self.LED_CHANNEL)
    self.lights.begin()

  def light_on(self, light, on):

    if on == True:
      self.lights.setPixelColor(light, Color(255, 255, 255))
      self.lights.show()
    else:
      self.lights.setPixelColor(light, Color(0, 0, 0))
      self.lights.show()

  def lights_off(self):
    self.light_on(0, False);
    self.light_on(1, False);
    self.light_on(2, False);

  def wheel(self, pos):
    if pos < 85:
        return Color(pos * 3, 255 - pos * 3, 0)
    elif pos < 170:
        pos -= 85
        return Color(255 - pos * 3, 0, pos * 3)
    else:
        pos -= 170
        return Color(0, pos * 3, 255 - pos * 3)

  def cycle_light_colours(self, light, user_input=False):
    
    for j in range(256):
      self.lights.setPixelColor(light, self.wheel((1+j) & 255))
      self.lights.show()
      if user_input:
        raw_input("Press Enter for next colour...")
      else:
        time.sleep(20/1000.0)    

  def light_party(self, iterations=1):
    
    for i in range(3):
      self.cycle_light_colours(i)
      self.lights_off()
      
    # cur_light = 0

    # for j in range(256*iterations):
    #   self.lights.setPixelColor(cur_light, self.wheel((1+j) & 255))
    #   self.lights.show()
    #   time.sleep(0.5)    
    #   # time.sleep(2)    
    #   self.light_on(cur_light, False)
    #   cur_light = cur_light + 1
    #   if cur_light == 3:
    #     cur_light = 0

if __name__ == '__main__':
    main()