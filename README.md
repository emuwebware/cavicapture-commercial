Increase GPU memory (otherwise picamera won’t be able to do high resolution image capture)

`sudo raspi-config`

Change "Memory Split" under "Advanced Options" to 256 (i.e. 256mb)
